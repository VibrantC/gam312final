// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	C++ class header boilerplate exported from UnrealHeaderTool.
	This is automatically generated by the tools.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAM312FINAL_MyCharacter_generated_h
#error "MyCharacter.generated.h already included, missing '#pragma once' in MyCharacter.h"
#endif
#define GAM312FINAL_MyCharacter_generated_h

#define GAM312Final_Source_GAM312Final_MyCharacter_h_14_RPC_WRAPPERS
#define GAM312Final_Source_GAM312Final_MyCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define GAM312Final_Source_GAM312Final_MyCharacter_h_14_INCLASS_NO_PURE_DECLS \
	private: \
	static void StaticRegisterNativesAMyCharacter(); \
	friend GAM312FINAL_API class UClass* Z_Construct_UClass_AMyCharacter(); \
	public: \
	DECLARE_CLASS(AMyCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAM312Final"), NO_API) \
	DECLARE_SERIALIZER(AMyCharacter) \
	/** Indicates whether the class is compiled into the engine */ \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM312Final_Source_GAM312Final_MyCharacter_h_14_INCLASS \
	private: \
	static void StaticRegisterNativesAMyCharacter(); \
	friend GAM312FINAL_API class UClass* Z_Construct_UClass_AMyCharacter(); \
	public: \
	DECLARE_CLASS(AMyCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAM312Final"), NO_API) \
	DECLARE_SERIALIZER(AMyCharacter) \
	/** Indicates whether the class is compiled into the engine */ \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM312Final_Source_GAM312Final_MyCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyCharacter(AMyCharacter&&); \
	NO_API AMyCharacter(const AMyCharacter&); \
public:


#define GAM312Final_Source_GAM312Final_MyCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyCharacter(AMyCharacter&&); \
	NO_API AMyCharacter(const AMyCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyCharacter)


#define GAM312Final_Source_GAM312Final_MyCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SpringArm() { return STRUCT_OFFSET(AMyCharacter, SpringArm); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(AMyCharacter, FollowCamera); }


#define GAM312Final_Source_GAM312Final_MyCharacter_h_11_PROLOG
#define GAM312Final_Source_GAM312Final_MyCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM312Final_Source_GAM312Final_MyCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	GAM312Final_Source_GAM312Final_MyCharacter_h_14_RPC_WRAPPERS \
	GAM312Final_Source_GAM312Final_MyCharacter_h_14_INCLASS \
	GAM312Final_Source_GAM312Final_MyCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAM312Final_Source_GAM312Final_MyCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM312Final_Source_GAM312Final_MyCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	GAM312Final_Source_GAM312Final_MyCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	GAM312Final_Source_GAM312Final_MyCharacter_h_14_INCLASS_NO_PURE_DECLS \
	GAM312Final_Source_GAM312Final_MyCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAM312Final_Source_GAM312Final_MyCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
