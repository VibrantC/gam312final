// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Boilerplate C++ definitions for a single module.
	This is automatically generated by UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "GAM312Final.h"
#include "GAM312Final.generated.dep.h"
PRAGMA_DISABLE_OPTIMIZATION
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode1GAM312Final() {}
	void UBTService_CheckForPlayer::StaticRegisterNativesUBTService_CheckForPlayer()
	{
	}
	IMPLEMENT_CLASS(UBTService_CheckForPlayer, 1454053792);
	void ACameraControl::StaticRegisterNativesACameraControl()
	{
	}
	IMPLEMENT_CLASS(ACameraControl, 1607761347);
	void AEnemyAI::StaticRegisterNativesAEnemyAI()
	{
	}
	IMPLEMENT_CLASS(AEnemyAI, 3921461912);
	void AEnemyAIController::StaticRegisterNativesAEnemyAIController()
	{
	}
	IMPLEMENT_CLASS(AEnemyAIController, 339118050);
	void AGAM312FinalGameModeBase::StaticRegisterNativesAGAM312FinalGameModeBase()
	{
	}
	IMPLEMENT_CLASS(AGAM312FinalGameModeBase, 67160662);
	void APickupComponent::StaticRegisterNativesAPickupComponent()
	{
		FNativeFunctionRegistrar::RegisterFunction(APickupComponent::StaticClass(), "OnPlayerEnterPickupBox",(Native)&APickupComponent::execOnPlayerEnterPickupBox);
	}
	IMPLEMENT_CLASS(APickupComponent, 3238369484);
	void AMyCharacter::StaticRegisterNativesAMyCharacter()
	{
	}
	IMPLEMENT_CLASS(AMyCharacter, 54093872);
	void AMyGameMode::StaticRegisterNativesAMyGameMode()
	{
	}
	IMPLEMENT_CLASS(AMyGameMode, 3204600039);
	void ASolidBodyComponent::StaticRegisterNativesASolidBodyComponent()
	{
	}
	IMPLEMENT_CLASS(ASolidBodyComponent, 3512200532);
#if USE_COMPILED_IN_NATIVES
// Cross Module References
	AIMODULE_API class UClass* Z_Construct_UClass_UBTService();
	ENGINE_API class UClass* Z_Construct_UClass_AActor();
	ENGINE_API class UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_ACharacter();
	AIMODULE_API class UClass* Z_Construct_UClass_UBehaviorTree_NoRegister();
	AIMODULE_API class UClass* Z_Construct_UClass_AAIController();
	AIMODULE_API class UClass* Z_Construct_UClass_UBlackboardComponent_NoRegister();
	AIMODULE_API class UClass* Z_Construct_UClass_UBehaviorTreeComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_AGameModeBase();
	ENGINE_API class UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	ENGINE_API class UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_APawn();
	ENGINE_API class UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_UShapeComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_AGameMode();

	GAM312FINAL_API class UClass* Z_Construct_UClass_UBTService_CheckForPlayer_NoRegister();
	GAM312FINAL_API class UClass* Z_Construct_UClass_UBTService_CheckForPlayer();
	GAM312FINAL_API class UClass* Z_Construct_UClass_ACameraControl_NoRegister();
	GAM312FINAL_API class UClass* Z_Construct_UClass_ACameraControl();
	GAM312FINAL_API class UClass* Z_Construct_UClass_AEnemyAI_NoRegister();
	GAM312FINAL_API class UClass* Z_Construct_UClass_AEnemyAI();
	GAM312FINAL_API class UClass* Z_Construct_UClass_AEnemyAIController_NoRegister();
	GAM312FINAL_API class UClass* Z_Construct_UClass_AEnemyAIController();
	GAM312FINAL_API class UClass* Z_Construct_UClass_AGAM312FinalGameModeBase_NoRegister();
	GAM312FINAL_API class UClass* Z_Construct_UClass_AGAM312FinalGameModeBase();
	GAM312FINAL_API class UFunction* Z_Construct_UFunction_APickupComponent_OnPlayerEnterPickupBox();
	GAM312FINAL_API class UClass* Z_Construct_UClass_APickupComponent_NoRegister();
	GAM312FINAL_API class UClass* Z_Construct_UClass_APickupComponent();
	GAM312FINAL_API class UClass* Z_Construct_UClass_AMyCharacter_NoRegister();
	GAM312FINAL_API class UClass* Z_Construct_UClass_AMyCharacter();
	GAM312FINAL_API class UClass* Z_Construct_UClass_AMyGameMode_NoRegister();
	GAM312FINAL_API class UClass* Z_Construct_UClass_AMyGameMode();
	GAM312FINAL_API class UClass* Z_Construct_UClass_ASolidBodyComponent_NoRegister();
	GAM312FINAL_API class UClass* Z_Construct_UClass_ASolidBodyComponent();
	GAM312FINAL_API class UPackage* Z_Construct_UPackage__Script_GAM312Final();
	UClass* Z_Construct_UClass_UBTService_CheckForPlayer_NoRegister()
	{
		return UBTService_CheckForPlayer::StaticClass();
	}
	UClass* Z_Construct_UClass_UBTService_CheckForPlayer()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UBTService();
			Z_Construct_UPackage__Script_GAM312Final();
			OuterClass = UBTService_CheckForPlayer::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20100080;


				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("BTService_CheckForPlayer.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("BTService_CheckForPlayer.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBTService_CheckForPlayer(Z_Construct_UClass_UBTService_CheckForPlayer, &UBTService_CheckForPlayer::StaticClass, TEXT("UBTService_CheckForPlayer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBTService_CheckForPlayer);
	UClass* Z_Construct_UClass_ACameraControl_NoRegister()
	{
		return ACameraControl::StaticClass();
	}
	UClass* Z_Construct_UClass_ACameraControl()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AActor();
			Z_Construct_UPackage__Script_GAM312Final();
			OuterClass = ACameraControl::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_ThirdPersonCamera = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ThirdPersonCamera"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(ThirdPersonCamera, ACameraControl), 0x0010000000000001, Z_Construct_UClass_AActor_NoRegister());
				UProperty* NewProp_FirstPersonCamera = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("FirstPersonCamera"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(FirstPersonCamera, ACameraControl), 0x0010000000000001, Z_Construct_UClass_AActor_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("CameraControl.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("CameraControl.h"));
				MetaData->SetValue(NewProp_ThirdPersonCamera, TEXT("Category"), TEXT("CameraControl"));
				MetaData->SetValue(NewProp_ThirdPersonCamera, TEXT("ModuleRelativePath"), TEXT("CameraControl.h"));
				MetaData->SetValue(NewProp_FirstPersonCamera, TEXT("Category"), TEXT("CameraControl"));
				MetaData->SetValue(NewProp_FirstPersonCamera, TEXT("ModuleRelativePath"), TEXT("CameraControl.h"));
				MetaData->SetValue(NewProp_FirstPersonCamera, TEXT("ToolTip"), TEXT("Cameras"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACameraControl(Z_Construct_UClass_ACameraControl, &ACameraControl::StaticClass, TEXT("ACameraControl"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACameraControl);
	UClass* Z_Construct_UClass_AEnemyAI_NoRegister()
	{
		return AEnemyAI::StaticClass();
	}
	UClass* Z_Construct_UClass_AEnemyAI()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ACharacter();
			Z_Construct_UPackage__Script_GAM312Final();
			OuterClass = AEnemyAI::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_BehaviorTree = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("BehaviorTree"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(BehaviorTree, AEnemyAI), 0x0010000000000001, Z_Construct_UClass_UBehaviorTree_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Navigation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("EnemyAI.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("EnemyAI.h"));
				MetaData->SetValue(NewProp_BehaviorTree, TEXT("Category"), TEXT("AI"));
				MetaData->SetValue(NewProp_BehaviorTree, TEXT("ModuleRelativePath"), TEXT("EnemyAI.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AEnemyAI(Z_Construct_UClass_AEnemyAI, &AEnemyAI::StaticClass, TEXT("AEnemyAI"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AEnemyAI);
	UClass* Z_Construct_UClass_AEnemyAIController_NoRegister()
	{
		return AEnemyAIController::StaticClass();
	}
	UClass* Z_Construct_UClass_AEnemyAIController()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AAIController();
			Z_Construct_UPackage__Script_GAM312Final();
			OuterClass = AEnemyAIController::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900280;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_BlackboardComp = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("BlackboardComp"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(BlackboardComp, AEnemyAIController), 0x0040000000082008, Z_Construct_UClass_UBlackboardComponent_NoRegister());
				UProperty* NewProp_BehaviorTree = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("BehaviorTree"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(BehaviorTree, AEnemyAIController), 0x0040000000082008, Z_Construct_UClass_UBehaviorTreeComponent_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Collision Rendering Utilities|Transformation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("EnemyAIController.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("EnemyAIController.h"));
				MetaData->SetValue(NewProp_BlackboardComp, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_BlackboardComp, TEXT("ModuleRelativePath"), TEXT("EnemyAIController.h"));
				MetaData->SetValue(NewProp_BlackboardComp, TEXT("ToolTip"), TEXT("Our blackboard component"));
				MetaData->SetValue(NewProp_BehaviorTree, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_BehaviorTree, TEXT("ModuleRelativePath"), TEXT("EnemyAIController.h"));
				MetaData->SetValue(NewProp_BehaviorTree, TEXT("ToolTip"), TEXT("Our behavior tree component"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AEnemyAIController(Z_Construct_UClass_AEnemyAIController, &AEnemyAIController::StaticClass, TEXT("AEnemyAIController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AEnemyAIController);
	UClass* Z_Construct_UClass_AGAM312FinalGameModeBase_NoRegister()
	{
		return AGAM312FinalGameModeBase::StaticClass();
	}
	UClass* Z_Construct_UClass_AGAM312FinalGameModeBase()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AGameModeBase();
			Z_Construct_UPackage__Script_GAM312Final();
			OuterClass = AGAM312FinalGameModeBase::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900288;


				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("GAM312FinalGameModeBase.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("GAM312FinalGameModeBase.h"));
				MetaData->SetValue(OuterClass, TEXT("ShowCategories"), TEXT("Input|MouseInput Input|TouchInput"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGAM312FinalGameModeBase(Z_Construct_UClass_AGAM312FinalGameModeBase, &AGAM312FinalGameModeBase::StaticClass, TEXT("AGAM312FinalGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGAM312FinalGameModeBase);
	UFunction* Z_Construct_UFunction_APickupComponent_OnPlayerEnterPickupBox()
	{
		struct PickupComponent_eventOnPlayerEnterPickupBox_Parms
		{
			UPrimitiveComponent* OverlappedComp;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComp;
			int32 OtherBodyIndex;
			bool bFromSweep;
			FHitResult SweepResult;
		};
		UObject* Outer=Z_Construct_UClass_APickupComponent();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnPlayerEnterPickupBox"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00420401, 65535, sizeof(PickupComponent_eventOnPlayerEnterPickupBox_Parms));
			UProperty* NewProp_SweepResult = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("SweepResult"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(SweepResult, PickupComponent_eventOnPlayerEnterPickupBox_Parms), 0x0010008008000182, Z_Construct_UScriptStruct_FHitResult());
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(bFromSweep, PickupComponent_eventOnPlayerEnterPickupBox_Parms, bool);
			UProperty* NewProp_bFromSweep = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("bFromSweep"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bFromSweep, PickupComponent_eventOnPlayerEnterPickupBox_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(bFromSweep, PickupComponent_eventOnPlayerEnterPickupBox_Parms), sizeof(bool), true);
			UProperty* NewProp_OtherBodyIndex = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherBodyIndex"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(OtherBodyIndex, PickupComponent_eventOnPlayerEnterPickupBox_Parms), 0x0010000000000080);
			UProperty* NewProp_OtherComp = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherComp"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(OtherComp, PickupComponent_eventOnPlayerEnterPickupBox_Parms), 0x0010000000080080, Z_Construct_UClass_UPrimitiveComponent_NoRegister());
			UProperty* NewProp_OtherActor = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherActor"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(OtherActor, PickupComponent_eventOnPlayerEnterPickupBox_Parms), 0x0010000000000080, Z_Construct_UClass_AActor_NoRegister());
			UProperty* NewProp_OverlappedComp = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OverlappedComp"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(OverlappedComp, PickupComponent_eventOnPlayerEnterPickupBox_Parms), 0x0010000000080080, Z_Construct_UClass_UPrimitiveComponent_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("PickupComponent.h"));
			MetaData->SetValue(NewProp_SweepResult, TEXT("NativeConst"), TEXT(""));
			MetaData->SetValue(NewProp_OtherComp, TEXT("EditInline"), TEXT("true"));
			MetaData->SetValue(NewProp_OverlappedComp, TEXT("EditInline"), TEXT("true"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APickupComponent_NoRegister()
	{
		return APickupComponent::StaticClass();
	}
	UClass* Z_Construct_UClass_APickupComponent()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_APawn();
			Z_Construct_UPackage__Script_GAM312Final();
			OuterClass = APickupComponent::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;

				OuterClass->LinkChild(Z_Construct_UFunction_APickupComponent_OnPlayerEnterPickupBox());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_Mesh = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("Mesh"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(Mesh, APickupComponent), 0x0010000000080009, Z_Construct_UClass_UStaticMeshComponent_NoRegister());
				UProperty* NewProp_Collider = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("Collider"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(Collider, APickupComponent), 0x0010000000080009, Z_Construct_UClass_UShapeComponent_NoRegister());
				UProperty* NewProp_Root = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("Root"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(Root, APickupComponent), 0x0010000000080009, Z_Construct_UClass_USceneComponent_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_APickupComponent_OnPlayerEnterPickupBox(), "OnPlayerEnterPickupBox"); // 2468681587
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Navigation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("PickupComponent.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("PickupComponent.h"));
				MetaData->SetValue(NewProp_Mesh, TEXT("Category"), TEXT("PickupComponent"));
				MetaData->SetValue(NewProp_Mesh, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_Mesh, TEXT("ModuleRelativePath"), TEXT("PickupComponent.h"));
				MetaData->SetValue(NewProp_Collider, TEXT("Category"), TEXT("PickupComponent"));
				MetaData->SetValue(NewProp_Collider, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_Collider, TEXT("ModuleRelativePath"), TEXT("PickupComponent.h"));
				MetaData->SetValue(NewProp_Root, TEXT("Category"), TEXT("PickupComponent"));
				MetaData->SetValue(NewProp_Root, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_Root, TEXT("ModuleRelativePath"), TEXT("PickupComponent.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APickupComponent(Z_Construct_UClass_APickupComponent, &APickupComponent::StaticClass, TEXT("APickupComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APickupComponent);
	UClass* Z_Construct_UClass_AMyCharacter_NoRegister()
	{
		return AMyCharacter::StaticClass();
	}
	UClass* Z_Construct_UClass_AMyCharacter()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ACharacter();
			Z_Construct_UPackage__Script_GAM312Final();
			OuterClass = AMyCharacter::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_FollowCamera = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("FollowCamera"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(FollowCamera, AMyCharacter), 0x0040000000080009, Z_Construct_UClass_UCameraComponent_NoRegister());
				UProperty* NewProp_SpringArm = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SpringArm"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(SpringArm, AMyCharacter), 0x0040000000080009, Z_Construct_UClass_USpringArmComponent_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Navigation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("MyCharacter.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("MyCharacter.h"));
				MetaData->SetValue(NewProp_FollowCamera, TEXT("Category"), TEXT("MyCharacter"));
				MetaData->SetValue(NewProp_FollowCamera, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_FollowCamera, TEXT("ModuleRelativePath"), TEXT("MyCharacter.h"));
				MetaData->SetValue(NewProp_FollowCamera, TEXT("ToolTip"), TEXT("Create camera component"));
				MetaData->SetValue(NewProp_SpringArm, TEXT("Category"), TEXT("MyCharacter"));
				MetaData->SetValue(NewProp_SpringArm, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_SpringArm, TEXT("ModuleRelativePath"), TEXT("MyCharacter.h"));
				MetaData->SetValue(NewProp_SpringArm, TEXT("ToolTip"), TEXT("Create SpringArm component"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMyCharacter(Z_Construct_UClass_AMyCharacter, &AMyCharacter::StaticClass, TEXT("AMyCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyCharacter);
	UClass* Z_Construct_UClass_AMyGameMode_NoRegister()
	{
		return AMyGameMode::StaticClass();
	}
	UClass* Z_Construct_UClass_AMyGameMode()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AGameMode();
			Z_Construct_UPackage__Script_GAM312Final();
			OuterClass = AMyGameMode::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x2090028C;


				OuterClass->ClassConfigName = FName(TEXT("Game"));
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("MyGameMode.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("MyGameMode.h"));
				MetaData->SetValue(OuterClass, TEXT("ShowCategories"), TEXT("Input|MouseInput Input|TouchInput"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMyGameMode(Z_Construct_UClass_AMyGameMode, &AMyGameMode::StaticClass, TEXT("AMyGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyGameMode);
	UClass* Z_Construct_UClass_ASolidBodyComponent_NoRegister()
	{
		return ASolidBodyComponent::StaticClass();
	}
	UClass* Z_Construct_UClass_ASolidBodyComponent()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_APawn();
			Z_Construct_UPackage__Script_GAM312Final();
			OuterClass = ASolidBodyComponent::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;


				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Navigation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("SolidBodyComponent.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("SolidBodyComponent.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASolidBodyComponent(Z_Construct_UClass_ASolidBodyComponent, &ASolidBodyComponent::StaticClass, TEXT("ASolidBodyComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASolidBodyComponent);
	UPackage* Z_Construct_UPackage__Script_GAM312Final()
	{
		static UPackage* ReturnPackage = NULL;
		if (!ReturnPackage)
		{
			ReturnPackage = CastChecked<UPackage>(StaticFindObjectFast(UPackage::StaticClass(), NULL, FName(TEXT("/Script/GAM312Final")), false, false));
			ReturnPackage->SetPackageFlags(PKG_CompiledIn | 0x00000000);
			FGuid Guid;
			Guid.A = 0xC8B4A626;
			Guid.B = 0x31350B99;
			Guid.C = 0x00000000;
			Guid.D = 0x00000000;
			ReturnPackage->SetGuid(Guid);

		}
		return ReturnPackage;
	}
#endif

PRAGMA_ENABLE_DEPRECATION_WARNINGS
