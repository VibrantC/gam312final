// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	C++ class header boilerplate exported from UnrealHeaderTool.
	This is automatically generated by the tools.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAM312FINAL_Projectile_generated_h
#error "Projectile.generated.h already included, missing '#pragma once' in Projectile.h"
#endif
#define GAM312FINAL_Projectile_generated_h

#define GAM312Final_Source_GAM312Final_Projectile_h_11_RPC_WRAPPERS
#define GAM312Final_Source_GAM312Final_Projectile_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define GAM312Final_Source_GAM312Final_Projectile_h_11_INCLASS_NO_PURE_DECLS \
	private: \
	static void StaticRegisterNativesAProjectile(); \
	friend GAM312FINAL_API class UClass* Z_Construct_UClass_AProjectile(); \
	public: \
	DECLARE_CLASS(AProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAM312Final"), NO_API) \
	DECLARE_SERIALIZER(AProjectile) \
	/** Indicates whether the class is compiled into the engine */ \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM312Final_Source_GAM312Final_Projectile_h_11_INCLASS \
	private: \
	static void StaticRegisterNativesAProjectile(); \
	friend GAM312FINAL_API class UClass* Z_Construct_UClass_AProjectile(); \
	public: \
	DECLARE_CLASS(AProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAM312Final"), NO_API) \
	DECLARE_SERIALIZER(AProjectile) \
	/** Indicates whether the class is compiled into the engine */ \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM312Final_Source_GAM312Final_Projectile_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectile(AProjectile&&); \
	NO_API AProjectile(const AProjectile&); \
public:


#define GAM312Final_Source_GAM312Final_Projectile_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectile(AProjectile&&); \
	NO_API AProjectile(const AProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AProjectile)


#define GAM312Final_Source_GAM312Final_Projectile_h_11_PRIVATE_PROPERTY_OFFSET
#define GAM312Final_Source_GAM312Final_Projectile_h_8_PROLOG
#define GAM312Final_Source_GAM312Final_Projectile_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM312Final_Source_GAM312Final_Projectile_h_11_PRIVATE_PROPERTY_OFFSET \
	GAM312Final_Source_GAM312Final_Projectile_h_11_RPC_WRAPPERS \
	GAM312Final_Source_GAM312Final_Projectile_h_11_INCLASS \
	GAM312Final_Source_GAM312Final_Projectile_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAM312Final_Source_GAM312Final_Projectile_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM312Final_Source_GAM312Final_Projectile_h_11_PRIVATE_PROPERTY_OFFSET \
	GAM312Final_Source_GAM312Final_Projectile_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	GAM312Final_Source_GAM312Final_Projectile_h_11_INCLASS_NO_PURE_DECLS \
	GAM312Final_Source_GAM312Final_Projectile_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAM312Final_Source_GAM312Final_Projectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
