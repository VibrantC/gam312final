// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	C++ class header boilerplate exported from UnrealHeaderTool.
	This is automatically generated by the tools.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAM312FINAL_BTTask_MoveToPlayer_generated_h
#error "BTTask_MoveToPlayer.generated.h already included, missing '#pragma once' in BTTask_MoveToPlayer.h"
#endif
#define GAM312FINAL_BTTask_MoveToPlayer_generated_h

#define GAM312Final_Source_GAM312Final_BTTask_MoveToPlayer_h_14_RPC_WRAPPERS
#define GAM312Final_Source_GAM312Final_BTTask_MoveToPlayer_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define GAM312Final_Source_GAM312Final_BTTask_MoveToPlayer_h_14_INCLASS_NO_PURE_DECLS \
	private: \
	static void StaticRegisterNativesUBTTask_MoveToPlayer(); \
	friend GAM312FINAL_API class UClass* Z_Construct_UClass_UBTTask_MoveToPlayer(); \
	public: \
	DECLARE_CLASS(UBTTask_MoveToPlayer, UBTTask_BlackboardBase, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAM312Final"), NO_API) \
	DECLARE_SERIALIZER(UBTTask_MoveToPlayer) \
	/** Indicates whether the class is compiled into the engine */ \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM312Final_Source_GAM312Final_BTTask_MoveToPlayer_h_14_INCLASS \
	private: \
	static void StaticRegisterNativesUBTTask_MoveToPlayer(); \
	friend GAM312FINAL_API class UClass* Z_Construct_UClass_UBTTask_MoveToPlayer(); \
	public: \
	DECLARE_CLASS(UBTTask_MoveToPlayer, UBTTask_BlackboardBase, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAM312Final"), NO_API) \
	DECLARE_SERIALIZER(UBTTask_MoveToPlayer) \
	/** Indicates whether the class is compiled into the engine */ \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM312Final_Source_GAM312Final_BTTask_MoveToPlayer_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBTTask_MoveToPlayer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBTTask_MoveToPlayer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTTask_MoveToPlayer); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTTask_MoveToPlayer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTTask_MoveToPlayer(UBTTask_MoveToPlayer&&); \
	NO_API UBTTask_MoveToPlayer(const UBTTask_MoveToPlayer&); \
public:


#define GAM312Final_Source_GAM312Final_BTTask_MoveToPlayer_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBTTask_MoveToPlayer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTTask_MoveToPlayer(UBTTask_MoveToPlayer&&); \
	NO_API UBTTask_MoveToPlayer(const UBTTask_MoveToPlayer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTTask_MoveToPlayer); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTTask_MoveToPlayer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBTTask_MoveToPlayer)


#define GAM312Final_Source_GAM312Final_BTTask_MoveToPlayer_h_14_PRIVATE_PROPERTY_OFFSET
#define GAM312Final_Source_GAM312Final_BTTask_MoveToPlayer_h_11_PROLOG
#define GAM312Final_Source_GAM312Final_BTTask_MoveToPlayer_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM312Final_Source_GAM312Final_BTTask_MoveToPlayer_h_14_PRIVATE_PROPERTY_OFFSET \
	GAM312Final_Source_GAM312Final_BTTask_MoveToPlayer_h_14_RPC_WRAPPERS \
	GAM312Final_Source_GAM312Final_BTTask_MoveToPlayer_h_14_INCLASS \
	GAM312Final_Source_GAM312Final_BTTask_MoveToPlayer_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAM312Final_Source_GAM312Final_BTTask_MoveToPlayer_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM312Final_Source_GAM312Final_BTTask_MoveToPlayer_h_14_PRIVATE_PROPERTY_OFFSET \
	GAM312Final_Source_GAM312Final_BTTask_MoveToPlayer_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	GAM312Final_Source_GAM312Final_BTTask_MoveToPlayer_h_14_INCLASS_NO_PURE_DECLS \
	GAM312Final_Source_GAM312Final_BTTask_MoveToPlayer_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAM312Final_Source_GAM312Final_BTTask_MoveToPlayer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
