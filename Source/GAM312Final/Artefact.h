// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PickupComponent.h"
#include "Artefact.generated.h"

/**
 * 
 */
UCLASS()
class GAM312FINAL_API AArtefact : public APawn
{
	GENERATED_BODY()

public:

	AArtefact();

	UPROPERTY(EditAnywhere)
		USceneComponent* Root;
	UPROPERTY(EditAnywhere)
		UShapeComponent* Collider;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* Mesh;

	UFUNCTION()
	void OnPlayerEnterPickupBox(UPrimitiveComponent * OverlappedComp, AActor * OtherActor,
		UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
};
