// Fill out your copyright notice in the Description page of Project Settings.

#include "GAM312Final.h"
#include "MyCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "CameraControl.h"
#include "EngineUtils.h"
#include "PickupComponent.h"
#include "Weapon.h"
#include "Projectile.h"
#include "EnemyAI.h"
#include "Animation.h"
#include "Blueprint/UserWidget.h"


// Sets default values
AMyCharacter::AMyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	// Create SpringArm
	SpringArm = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
	SpringArm->SetupAttachment(RootComponent);
	SpringArm->TargetArmLength = 0.f; // The camera follows at this distance
	SpringArm->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create camera to follow character
	FollowCamera = CreateDefaultSubobject<UCameraComponent>("Camera");
	FollowCamera->SetupAttachment(SpringArm, USpringArmComponent::SocketName); // Attach the camera to the end of the Spring Arm
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate

	// Create weapon component and attach to character's weapon socket
	WeaponMesh = CreateDefaultSubobject<UStaticMeshComponent>("WeaponMesh");
	WeaponMesh->SetSimulatePhysics(false);
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	WeaponMesh->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, TEXT("RWeapon"));

	// Set weapon fire audio
	static ConstructorHelpers::FObjectFinder<USoundCue>RifleFire(TEXT("SoundCue'/Game/Sounds/RifleFire.RifleFire'"));
	RifleFireCue = RifleFire.Object;

	// Create rifle fire audio component
	RifleFireAudioComp = CreateDefaultSubobject<UAudioComponent>(TEXT("RifleFireAudioComp"));
	RifleFireAudioComp->bAutoActivate = false;
	RifleFireAudioComp->AttachToComponent(WeaponMesh, FAttachmentTransformRules::SnapToTargetNotIncludingScale, TEXT("Bullet"));

	// Set weapon reload audio
	static ConstructorHelpers::FObjectFinder<USoundCue>Reload(TEXT("SoundCue'/Game/Sounds/Reload.Reload'"));
	RifleReloadCue = Reload.Object;

	//// Create reload audio component
	//RifleReloadComp->CreateDefaultSubobject<UAudioComponent>(TEXT("RifleReloadAudioComp"));
	//RifleReloadComp->bAutoActivate = false;
	//RifleReloadComp->AttachToComponent(WeaponMesh, FAttachmentTransformRules::SnapToTargetNotIncludingScale, TEXT("Bullet"));

	UAnimation* Anim = Cast<UAnimation>(GetMesh()->GetAnimInstance());

	if (Anim)
	{
		Anim->Firing = false;
		Anim->TakingDamage = false;
	}
}

// Called when the game starts or when spawned
void AMyCharacter::BeginPlay()
{
	Super::BeginPlay();

	// initialize health
	InitialHealth = 100.0f;
	CurrentHealth = InitialHealth;

	// initialize medpacks
	InitialMedpacks = 1;
	CurrentMedpacks = InitialMedpacks;

	// initialize ammo
	InitialAmmo = 6.0f;
	CurrentAmmo = InitialAmmo;
	NeededAmmo = 0.f;
	ReloadAmmo = 0.f;
	TotalAmmo = 100.0f;

	// initialize weapon damage
	WeaponDamage = -10.f;

	// Initialize rifle fire audio component
	if (RifleFireCue->IsValidLowLevelFast())
	{
		RifleFireAudioComp->SetSound(RifleFireCue);
	}

	//// initialize rifle reload audio component
	//if (RifleReloadCue->IsValidLowLevelFast())
	//{
	//	RifleReloadComp->SetSound(RifleReloadCue);
	//}

	if (PlayerHUDClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), PlayerHUDClass);

		if (CurrentWidget != nullptr)
		{
			CurrentWidget->AddToViewport();
		}
	}
}

// Called every frame
void AMyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (CurrentHealth < 0)
	{
		CurrentHealth = 0;
	}
}

// Called to bind functionality to input
void AMyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// input controls for movement
	PlayerInputComponent->BindAxis("Lateral", this, &AMyCharacter::Lateral);
	PlayerInputComponent->BindAxis("SidetoSide", this, &AMyCharacter::SidetoSide);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AMyCharacter::OnStartJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AMyCharacter::OnStopJump);

	// input controls for mouselook
	PlayerInputComponent->BindAxis("Turn", this, &AMyCharacter::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &AMyCharacter::AddControllerPitchInput);
	
	// Input controls for sprint
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &AMyCharacter::BeginSprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &AMyCharacter::EndSprint);

	// Input controls for camera change
	PlayerInputComponent->BindAction("FirstPerson", IE_Pressed, this, &AMyCharacter::ChangeView<1>);
	PlayerInputComponent->BindAction("ThirdPerson", IE_Pressed, this, &AMyCharacter::ChangeView<2>);

	// Input controls for weapon firing
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AMyCharacter::Raycast);

	// Input controls for Use Item
	PlayerInputComponent->BindAction("UseItem", IE_Pressed, this, &AMyCharacter::UseItem);

	// Input controls for Reloading
	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &AMyCharacter::Reload);
}

// Called to add forward/backward motion to character
void AMyCharacter::Lateral(float Value)
{
	if (Controller && Value)
	{
		if (bIsSprinting)
		{
			Value *= 2; // allows character to move at maxwalkspeed set in UE4
		}

		AddMovementInput(GetActorForwardVector(), Value / 2);
	}
}

// Called to add right/left motion to character
void AMyCharacter::SidetoSide(float Value)
{
	if (Controller && Value)
	{
		AddMovementInput(GetActorRightVector(), Value / 2);
	}
}

// Called to add Jump capabilities to character
void AMyCharacter::OnStartJump()
{
	bPressedJump = true;
}

void AMyCharacter::OnStopJump()
{
	bPressedJump = false;
}

// Functions to handle beginning and ending of sprinting
void AMyCharacter::BeginSprint()
{
	bIsSprinting = true;
}
void AMyCharacter::EndSprint()
{
	bIsSprinting = false;
}

// Function to handle weapon firing
//void AMyCharacter::Fire()
//{
//	if (CurrentAmmo > 0)
//	{
//		GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::White, "Fire Called");
//
//		FVector SpawnLocation = WeaponMesh->GetSocketLocation(FName(TEXT("Bullet")));
//		FRotator SpawnRotation = FRotationMatrix::MakeFromX(SpawnLocation - WeaponMesh->GetSocketLocation(FName(TEXT("Bullet")))).Rotator();
//		WeaponMesh->GetSocketWorldLocationAndRotation("Bullet", SpawnLocation, SpawnRotation);
//		UWorld* World = GetWorld();
//		if (World != NULL)
//		{
//			World->SpawnActor<AProjectile>(SpawnLocation, SpawnRotation);
//		}
//		CurrentAmmo--;
//	}
//	else
//	{
//		return;
//	}
//}

void AMyCharacter::Raycast()
{
	FHitResult* HitResult = new FHitResult();
	FVector StartTrace = WeaponMesh->GetSocketLocation("Bullet");
	FVector ForwardVector = FollowCamera->GetForwardVector();
	FVector EndTrace = (ForwardVector * 5000.f) + StartTrace;
	FCollisionQueryParams* CQP = new FCollisionQueryParams();

	if (CurrentAmmo > 0)
	{
		//Play a sound when the weapon is fired
		RifleFireAudioComp->Play();

		// Subtract ammo from current ammo count every shot
		CurrentAmmo--;

		// Set up a ray trace from the end of the player's gun forward
		if (GetWorld()->LineTraceSingleByChannel(*HitResult, StartTrace, EndTrace, ECC_Visibility, *CQP))
		{
			if (HitResult->GetActor() != NULL)
			{
				// if the ray trace encounters an enemy, do damage to the enemy
				if (HitResult->GetActor()->IsA(AEnemyAI::StaticClass()))
				{
					AEnemyAI* Enemy = Cast<AEnemyAI>(HitResult->GetActor());
					if (Enemy)
					{
						Enemy->UpdateCurrentHealth(WeaponDamage);
					}
				}
			}
		}
	}
}

// Function to handle using item in inventory
void AMyCharacter::UseItem()
{
	if (CurrentHealth < InitialHealth)
	{
		if (CurrentMedpacks >= 1)
		{
			CurrentMedpacks--;
			CurrentHealth = CurrentHealth + 25;
		}
	}
}

// function to handle reloading weapon
void AMyCharacter::Reload()
{
	if (CurrentAmmo < InitialAmmo)
	{
		NeededAmmo = InitialAmmo - CurrentAmmo;

		if (TotalAmmo > NeededAmmo)
		{
			ReloadAmmo = NeededAmmo;
		}
		else
		{
			ReloadAmmo = TotalAmmo;
		}

		TotalAmmo = TotalAmmo - ReloadAmmo;

			CurrentAmmo = CurrentAmmo + ReloadAmmo;
			//CurrentAmmo++;
			//RifleReloadComp->Play();
	}

}

// Calls the correct camera function in CameraControl class based on which number was pressed
void AMyCharacter::ChangeView(int32 CamNumber)
{
	for (TActorIterator<ACameraControl> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		if (CamNumber == 1)
		{
			ActorItr->SetCameraOne();
			break;
		}
		else if (CamNumber == 2)
		{
			ActorItr->SetCameraTwo();
			break;
		}
	}
}

// returns player's starting health
float AMyCharacter::GetInitialHealth()
{
	return InitialHealth;
}

// returns player's current health
float AMyCharacter::GetCurrentHealth()
{
	return CurrentHealth;
}

// returns player's initial medpack count in inventory
float AMyCharacter::GetInitialMedpacks()
{
	return InitialMedpacks;
}

// returns player's current medpack count in inventory
float AMyCharacter::GetCurrentMedpacks()
{
	return CurrentMedpacks;
}

// called to update the player's medpack count in inventory
void AMyCharacter::UpdateCurrentMedpacks(float Medpacks)
{
	CurrentMedpacks = CurrentMedpacks + Medpacks;
}

// called to update player's current health
void AMyCharacter::UpdateCurrentHealth(float Health)
{
	CurrentHealth = CurrentHealth + Health;
}

// returns playar's starting ammo
float AMyCharacter::GetInitialAmmo()
{
	return InitialAmmo;
}

// returns player's current ammo
float AMyCharacter::GetCurrentAmmo()
{
	return CurrentAmmo;
}

// returns player's total ammo in inventory
float AMyCharacter::GetTotalAmmo()
{
	return TotalAmmo;
}

// called to update player's current ammo
void AMyCharacter::UpdateCurrentAmmo(float Ammo)
{
	CurrentAmmo = CurrentAmmo + Ammo;
}

// called to update player's total ammo in inventory
void AMyCharacter::UpdateTotalAmmo(float Ammo)
{
	TotalAmmo = TotalAmmo + Ammo;
}

float AMyCharacter::GetWeaponDamage()
{
	return WeaponDamage;
}

void AMyCharacter::OnPickupItem(FString Item)
{
	if (Item == "Medpack")
	{
		CurrentMedpacks += 1;
	}
	else if (Item == "Ammo")
	{
		TotalAmmo += 25;
	}
}




