// Fill out your copyright notice in the Description page of Project Settings.

#include "GAM312Final.h"
#include "PickupComponent.h"
#include "Engine/StaticMeshActor.h"
#include "MyCharacter.h"
#include "EnemyAI.h"


// Sets default values
APickupComponent::APickupComponent()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Scene Component
	Root = CreateDefaultSubobject<USceneComponent>("Root");
	RootComponent = Root;

	// Static Mesh Component
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	Mesh->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	Mesh->SetRelativeLocation(FVector(0.f, 0.f, -40.f));

	// Collider Component
	Collider = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollider"));
	Collider->SetWorldScale3D(FVector(1.f, 1.f, 1.f));
	Collider->bGenerateOverlapEvents = true;
	Collider->OnComponentBeginOverlap.AddDynamic(this, &APickupComponent::OnPlayerEnterPickupBox);
	Collider->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
}

// Called when the game starts or when spawned
void APickupComponent::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APickupComponent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APickupComponent::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


// Called to handle overlap events
void APickupComponent::OnPlayerEnterPickupBox(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, 
	UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	Destroy();
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Item added to inventory"));

	AMyCharacter* Player = Cast<AMyCharacter>(OtherActor);

	if (Player)
	{
		Player->OnPickupItem(ItemName);
	}
}

