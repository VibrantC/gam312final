// Fill out your copyright notice in the Description page of Project Settings.

#include "GAM312Final.h"
#include "Artefact.h"


AArtefact::AArtefact()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Scene Component
	Root = CreateDefaultSubobject<USceneComponent>("Root");
	RootComponent = Root;

	// Static Mesh Component
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	Mesh->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	Mesh->SetRelativeLocation(FVector(0.f, 0.f, -40.f));

	// Collider Component
	Collider = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollider"));
	Collider->SetWorldScale3D(FVector(1.f, 1.f, 1.f));
	Collider->bGenerateOverlapEvents = true;
	Collider->OnComponentBeginOverlap.AddDynamic(this, &AArtefact::OnPlayerEnterPickupBox);
	Collider->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
}


void AArtefact::OnPlayerEnterPickupBox(UPrimitiveComponent * OverlappedComp, AActor * OtherActor,
	UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	UGameplayStatics::OpenLevel(this, FName(TEXT("World'/Game/Maps/EndLevel.EndLevel'")), false, TEXT(""));
}
