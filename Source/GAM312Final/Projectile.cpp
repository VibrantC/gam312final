// Fill out your copyright notice in the Description page of Project Settings.

#include "GAM312Final.h"
#include "Projectile.h"


// Sets default values
AProjectile::AProjectile()
{
// 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
//	PrimaryActorTick.bCanEverTick = true;
//
//	// Create the collision sphere component
//	CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
//	CollisionComponent->InitSphereRadius(15.0f);
//	CollisionComponent->bGenerateOverlapEvents = true;
//	RootComponent = CollisionComponent;
//
//	// Create static mesh component and attach it to the root
//	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
//	Mesh->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
//	static ConstructorHelpers::FObjectFinder<UStaticMesh>SphereVisualAsset(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere'"));
//	if (SphereVisualAsset.Succeeded())
//	{
//		Mesh->SetStaticMesh(SphereVisualAsset.Object);
//		Mesh->SetRelativeLocation(FVector(0));
//		Mesh->SetWorldScale3D(FVector(0.05f));
//		Mesh->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
//	}
//
//	// Create the movement component
//	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
//	ProjectileMovementComponent->SetUpdatedComponent(CollisionComponent);
//	ProjectileMovementComponent->InitialSpeed = 3000.f;
//	ProjectileMovementComponent->MaxSpeed = 3000.f;
//	ProjectileMovementComponent->bRotationFollowsVelocity = true;
//	ProjectileMovementComponent->bShouldBounce = false;
//}
//
//// Called when the game starts or when spawned
//void AProjectile::BeginPlay()
//{
//	Super::BeginPlay();
//	
//	BulletDamage = -10;
//}
//
//// Called every frame
//void AProjectile::Tick(float DeltaTime)
//{
//	Super::Tick(DeltaTime);
//}
//
//void AProjectile::FireInDirection(const FVector & ShootDirection)
//{
//	ProjectileMovementComponent->Velocity = ShootDirection * ProjectileMovementComponent->InitialSpeed;
//}
//
//float AProjectile::GetBulletDamage()
//{
//	return BulletDamage;
}

