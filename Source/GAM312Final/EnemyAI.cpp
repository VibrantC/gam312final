// Fill out your copyright notice in the .fscription page of Project Settings.

#include "GAM312Final.h"
#include "EnemyAI.h"
#include "EnemyAIController.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Perception/PawnSensingComponent.h"
#include "MyCharacter.h"
#include "Projectile.h"
#include "Animation.h"



// Sets default values
AEnemyAI::AEnemyAI()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true; 
	PrimaryActorTick.bAllowTickOnDedicatedServer = true;

	// Initialize senses
	PawnSensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComponent"));
	PawnSensingComp->SetPeripheralVisionAngle(90.f);
	PawnSensingComp->SetSensingUpdatesEnabled(true);

	AttackRangeBox = CreateDefaultSubobject<UBoxComponent>(TEXT("TriggerBox"));
	AttackRangeBox->SetWorldScale3D(FVector(10.f));
	AttackRangeBox->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	AttackRangeBox->bGenerateOverlapEvents = true;
}

// Called when the game starts or when spawned
void AEnemyAI::BeginPlay()
{
	Super::BeginPlay();

	if (PawnSensingComp)
	{
		PawnSensingComp->OnSeePawn.AddDynamic(this, &AEnemyAI::OnSeePlayer);
	}

	if (AttackRangeBox)
	{
		AttackRangeBox->OnComponentBeginOverlap.AddDynamic(this, &AEnemyAI::OnEnterRange);
		AttackRangeBox->OnComponentEndOverlap.AddDynamic(this, &AEnemyAI::OnExitRange);
	}

	InitialHealth = 100.f;
	CurrentHealth = InitialHealth;
	EnemyDamage = -5.f;
}

// Called every frame
void AEnemyAI::Tick(float DeltaTime)
{	
	Super::Tick(DeltaTime);
}

void AEnemyAI::OnSeePlayer(APawn * Pawn)
{
	//Get a reference to the player controller
	AEnemyAIController* AIController = Cast<AEnemyAIController>(GetController());

	if (AIController)
	{
		AIController->SetSeenTarget(Pawn);

		UAnimation* Anim = Cast<UAnimation>(GetMesh()->GetAnimInstance());
		if (Anim)
		{
			Anim->Speed = 150;
		}
	}
}

// returns initial health
float AEnemyAI::GetInitialHealth()
{
	return InitialHealth;
}

// returns current health
float AEnemyAI::GetCurrentHealth()
{
	return CurrentHealth;
}

// Called to update health
void AEnemyAI::UpdateCurrentHealth(float Health)
{
	CurrentHealth = CurrentHealth + Health;

	// Plays a death animation and destroys the actor after health reaches 0
	if (CurrentHealth <= 0)
	{
		AEnemyAIController* AIController = Cast<AEnemyAIController>(GetController());
		if (AIController)
		{
			AIController->BehaviorComp->StopTree();
		}

		UAnimation* Anim = Cast<UAnimation>(GetMesh()->GetAnimInstance());
		if (Anim)
		{
			Anim->Dead = true;
		}
		GetWorld()->GetTimerManager().SetTimer(DeathDelayTH, this, &AEnemyAI::EnemyDeath, 3.3f, false);
	}
}

// Destroy Enemy actor
void AEnemyAI::EnemyDeath()
{
	this->Destroy();
	GetWorldTimerManager().ClearTimer(DeathDelayTH);
}

// returns enemy damage
float AEnemyAI::GetEnemyDamage()
{
	return EnemyDamage;
}

void AEnemyAI::OnEnterRange(UPrimitiveComponent * OverlappedComp, AActor * OtherActor,
	UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	AMyCharacter* Char = Cast<AMyCharacter>(OtherActor);

	if (Char)
	{
		bIsInRange = true;
		AttackPlayer();
	}
}

void AEnemyAI::OnExitRange(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	bIsInRange = false;
	GetWorldTimerManager().ClearTimer(AttackTH);
	UAnimation* Anim = Cast<UAnimation>(GetMesh()->GetAnimInstance());
	if (Anim)
	{
		Anim->Attacking = false;
	}

}

void AEnemyAI::AttackPlayer()
{
	if (CurrentHealth > 0)
	{
		AMyCharacter* Char = Cast <AMyCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());

		UAnimation* Anim = Cast<UAnimation>(GetMesh()->GetAnimInstance());
		if (Anim)
		{
			Anim->Attacking = true;
		}

		if (bIsInRange)
		{

			GetWorld()->GetTimerManager().SetTimer(AttackTH, this, &AEnemyAI::AttackPlayer, 2.8f, false);
			Char->UpdateCurrentHealth(EnemyDamage);

		}
	}
}
