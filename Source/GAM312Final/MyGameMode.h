// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "MyGameMode.generated.h"

/**
 * 
 */
UCLASS()
class GAM312FINAL_API AMyGameMode : public AGameMode
{
	GENERATED_BODY()

	virtual void BeginPlay() override;

public:
	AMyGameMode();

protected:

	class AMyCharacter * MyCharacter;

	//UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Stamina", Meta = (BlueprintProtected = "true"))
	//TSubclassOf<class UUserWidget> PlayerHUDClass;

	//// Allows for cycling through possible widgets
	//UPROPERTY()
	//class UUserWidget* CurrentWidget;
	
};
