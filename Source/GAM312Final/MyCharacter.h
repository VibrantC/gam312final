// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Components/InputComponent.h"
#include "Camera/CameraComponent.h"
#include "PickupComponent.h"
#include "MyCharacter.generated.h"


UCLASS()
class GAM312FINAL_API AMyCharacter : public ACharacter
{
	GENERATED_BODY()

	// Create SpringArm component
	UPROPERTY(EditAnywhere)
		class USpringArmComponent* SpringArm;

	// Create camera component
	UPROPERTY(EditAnywhere)
		class UCameraComponent* FollowCamera;

	UPROPERTY(EditAnywhere)
		class UStaticMeshComponent* WeaponMesh;

	// create audio cues
	UPROPERTY(EditAnywhere, Category = "Audio")
		USoundCue* RifleFireCue;
	UPROPERTY(EditAnywhere, Category = "Audio")
		USoundCue* RifleReloadCue;
	UPROPERTY(EditAnywhere, Category = "Audio")
		USoundCue* AmbientSoundCue;

	// create audio components
	UPROPERTY(EditAnywhere, Category = "Audio")
		UAudioComponent* RifleFireAudioComp;
	UPROPERTY(EditAnywhere)
		UAudioComponent* RifleReloadComp;
	UPROPERTY(EditAnywhere)
		UAudioComponent* AmbientSoundComp;


public:
	// Sets default values for this character's properties
	AMyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	// variables for player health
	UPROPERTY(EditAnywhere, Category = "Health")
	float InitialHealth;

	UPROPERTY(EditAnywhere, Category = "Health")
	float CurrentHealth;

	UPROPERTY(EditAnywhere, Category = "Health")
	float InitialMedpacks;

	UPROPERTY(EditAnywhere, Category = "Health")
	float CurrentMedpacks;

	// variables for player ammo
	UPROPERTY(EditAnywhere, Category = "Ammo")
	float InitialAmmo;

	UPROPERTY(EditAnywhere, Category = "Ammo")
	float CurrentAmmo;

	UPROPERTY(EditAnywhere, Category = "Ammo")
	float NeededAmmo;

	UPROPERTY(EditAnywhere, Category = "Ammo")
	float ReloadAmmo;

	UPROPERTY(EditAnywhere, Category = "Ammo")
	float TotalAmmo;

	// variables for player damage
	UPROPERTY(EditAnywhere, Category = "Damage")
	float WeaponDamage;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Called to set movement based on input
	void Lateral(float Value);
	void SidetoSide(float Value);
	void OnStartJump();
	void OnStopJump();

	// Handles player sprinting
	void BeginSprint();
	void EndSprint();

	bool bIsSprinting = false;

	// Handles weapon firing
	void Fire();

	// Handles raycasting
	void Raycast();

	// Handles using health item
	void UseItem();

	// Handles reloading weapon
	void Reload();

	// Set up Camera Change function
	void ChangeView(int32 CamNumber);
	template<int32 CamNumber>
	void ChangeView()
	{
		ChangeView(CamNumber);
	}

	// Functions for accessing and controlling player health
	UFUNCTION(BlueprintPure, Category = "Health")
	float GetInitialHealth();

	UFUNCTION(BlueprintPure, Category = "Health")
	float GetCurrentHealth();

	UFUNCTION(BlueprintPure, Category = "Health")
	float GetInitialMedpacks();

	UFUNCTION(BlueprintPure, Category = "Health")
	float GetCurrentMedpacks();

	// Updates the player's medpacks in inventory
	// @param Medpacks This is the amount to change a player's medpack count in inventory. It can be positive or negative
	UFUNCTION(BlueprintCallable, Category = "Health")
	void UpdateCurrentMedpacks(float Medpacks);

	// Updates the player's current health
	// @param Health This is the amount to change a player's health by. It can be positive or negative
	UFUNCTION(BlueprintCallable, Category = "Health")
	void UpdateCurrentHealth(float Stamina);

	// Functions for accessing and controlling player ammo
	UFUNCTION(BlueprintPure, Category = "Ammo")
	float GetInitialAmmo();

	UFUNCTION(BlueprintPure, Category = "Ammo")
	float GetCurrentAmmo();

	UFUNCTION(BlueprintPure, Category = "Ammo")
		float GetTotalAmmo();

	// Updates the player's current ammo
	// @param Ammo This is the amount to change a player's ammo by. It can be positive or negative
	UFUNCTION(BlueprintCallable, Category = "Ammo")
	void UpdateCurrentAmmo(float Ammo);

	// Updates the player's total ammo in inventory
	// @param TotalAmmo This is the amount to change a player's current total ammo in inventory. It can be positive or negative
	UFUNCTION(BlueprintCallable, Category = "Ammo")
	void UpdateTotalAmmo(float Ammo);

	UFUNCTION(BlueprintCallable, Category = "Damage")
		float GetWeaponDamage();

	// Handles item management
	void OnPickupItem(FString Item);

	UPROPERTY()
	class UUserWidget* CurrentWidget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Stamina", Meta = (BlueprintProtected = "true"))
	TSubclassOf<class UUserWidget> PlayerHUDClass;

};
