// Fill out your copyright notice in the Description page of Project Settings.

#include "GAM312Final.h"
#include "BTTask_MoveToPlayer.h"
#include "EnemyAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "EnemyAI.h"
#include "EnemyAIController.h"
#include "MyCharacter.h"


//EBTNodeResult::Type UBTTask_MoveToPlayer::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
//{
//	// Get player controller
//	AEnemyAIController *CharPC = Cast<AEnemyAIController>(OwnerComp.GetAIOwner());
//
//	// Get character and cast from the blackboard value
//	AMyCharacter *Enemy = Cast<AMyCharacter>(OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>(CharPC->EnemyKeyID));
//
//	// if there is an enemy(the player), move to it
//	if (Enemy)
//	{
//		CharPC->MoveToActor(Enemy, 5.f, true, true, true, 0, true);
//		return EBTNodeResult::Succeeded;
//	}
//	else
//	{
//		return EBTNodeResult::Failed;
//	}
//
//	EBTNodeResult::Failed;
//}
