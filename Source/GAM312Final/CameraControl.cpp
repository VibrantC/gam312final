// Fill out your copyright notice in the Description page of Project Settings.

#include "GAM312Final.h"
#include "CameraControl.h"
#include "Components/InputComponent.h"
#include "MyCharacter.h"


// Sets default values
ACameraControl::ACameraControl()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void ACameraControl::BeginPlay()
{
	Super::BeginPlay();
	MyPlayerController = UGameplayStatics::GetPlayerController(this, 0);
}

void ACameraControl::SetCameraOne()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Camera Change Called"));

	if (MyPlayerController)
	{
		if (MyPlayerController->GetViewTarget() != FirstPersonCamera)
		{
			MyPlayerController->SetViewTargetWithBlend(FirstPersonCamera, .75f);
		}
	}
}

void ACameraControl::SetCameraTwo()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Camera Change Called"));

	if (MyPlayerController)
	{
		if (MyPlayerController->GetViewTarget() != ThirdPersonCamera)
		{
			MyPlayerController->SetViewTargetWithBlend(ThirdPersonCamera, .75f);
		}
	}
	
}

