// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

UCLASS()
class GAM312FINAL_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectile();
//
//	// Sphere collision component
//	UPROPERTY(VisibleDefaultsOnly, Category = "Projectile")
//	USphereComponent* CollisionComponent;
//
//	// Static mesh component
//	UPROPERTY(EditAnywhere)
//	UStaticMeshComponent* Mesh;
//
//	// movement component
//	UPROPERTY(VisibleAnywhere, Category = "Movement")
//	UProjectileMovementComponent* ProjectileMovementComponent;
//
//protected:
//	// Called when the game starts or when spawned
//	virtual void BeginPlay() override;
//
//public:	
//	// Called every frame
//	virtual void Tick(float DeltaTime) override;
//
//	// Function that initializes the projectile's velocity in the shoot direction
//	void FireInDirection(const FVector& ShootDirection);
//
//	// Damage accessor function
//	UFUNCTION(BlueprintCallable, Category = "Damage")
//		float GetBulletDamage();
//	
//private:
//	UPROPERTY(EditAnywhere, Category = "Damage")
//		float BulletDamage;

};
