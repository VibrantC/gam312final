// Fill out your copyright notice in the Description page of Project Settings.

#include "GAM312Final.h"
#include "BTService_CheckForPlayer.h"
#include "EnemyAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "EnemyAI.h"
#include "MyCharacter.h"


UBTService_CheckForPlayer::UBTService_CheckForPlayer()
{
	bCreateNodeInstance = true;
}

//// Makes a check for the player character every tick ***Should have some call to pawn sensing***
//void UBTService_CheckForPlayer::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
//{
//	//// get the enemyAI controller
//	//AEnemyAIController* EnemyPC = Cast<AEnemyAIController>(OwnerComp.GetAIOwner());
//
//	//AEnemyAI* Char = Cast<AEnemyAI>(OwnerComp.GetOwner());
//
//	//if (EnemyPC)
//	//{
//	//	// gets the player controller for the player character
//	//	AMyCharacter* Enemy = Cast<AMyCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
//
//	//	// if the player exists, set it as the target
//	//	if (Enemy)
//	//	{
//	//		OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>(EnemyPC->EnemyKeyID, Enemy);
//	//	}
//	//}
//}