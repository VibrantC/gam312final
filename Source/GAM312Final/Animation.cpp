// Fill out your copyright notice in the Description page of Project Settings.

#include "GAM312Final.h"
#include "Animation.h"




UAnimation::UAnimation(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	TakingDamage = false;

	Firing = false;

	Dead = false;

	Attacking = false;

	Speed = 0.f;
}