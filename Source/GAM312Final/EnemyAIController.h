// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "EnemyAIController.generated.h"

/**
 * 
 */
UCLASS()
class GAM312FINAL_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()



public:
	AEnemyAIController();

	// Our behavior tree component
	UPROPERTY(transient)
	class UBehaviorTreeComponent* BehaviorComp;

	// Our blackboard component
	UPROPERTY(transient)
	class UBlackboardComponent* BlackboardComp;

	virtual void Possess(APawn* Pawn) override;

	void SetSeenTarget(APawn* Pawn);

	// blackboard key
	UPROPERTY(EditDefaultsOnly, Category = "AI")
	FName PlayerKey = "Target";	
};
