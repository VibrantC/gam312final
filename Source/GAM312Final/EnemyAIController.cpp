// Fill out your copyright notice in the Description page of Project Settings.

#include "GAM312Final.h"
#include "EnemyAIController.h"
#include "AIPatrolPoint.h"
#include "Kismet/GameplayStatics.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "EnemyAI.h"
#include "MyCharacter.h"

AEnemyAIController::AEnemyAIController()
{
	// Initialized blackboard and behavior tree
	BehaviorComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));
	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComp"));
}

void AEnemyAIController::Possess(APawn * Pawn)
{
	Super::Possess(Pawn);

	// Get reference to the character
	AEnemyAI* Char = Cast<AEnemyAI>(Pawn);

	if (Char)
	{
		if (Char->BehaviorTree->BlackboardAsset)
		{
			// initialize the blackboard asset to the behavior tree component
			BlackboardComp->InitializeBlackboard(*Char->BehaviorTree->BlackboardAsset);

			// Tells the Behavior tree to run
			BehaviorComp->StartTree(*Char->BehaviorTree);
		}
	}
}

void AEnemyAIController::SetSeenTarget(APawn * Pawn)
{
	if (BlackboardComp)
	{
		BlackboardComp->SetValueAsObject(PlayerKey, Pawn);

	}
}

