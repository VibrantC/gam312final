// Fill out your copyright notice in the Description page of Project Settings.

#include "GAM312Final.h"
#include "SolidBodyComponent.h"


// Sets default values
ASolidBodyComponent::ASolidBodyComponent()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}
// Called when the game starts or when spawned
void ASolidBodyComponent::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASolidBodyComponent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASolidBodyComponent::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

