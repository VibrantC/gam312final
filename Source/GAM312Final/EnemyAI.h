// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "EnemyAI.generated.h"

UCLASS()
class GAM312FINAL_API AEnemyAI : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemyAI();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	// The component which is used for the seeing sense of the AI
	UPROPERTY(VisibleAnywhere, Category = "AI")
	class UPawnSensingComponent* PawnSensingComp;

	// The behavior tree of the character
	UPROPERTY(EditAnywhere, Category = "AI")
	class UBehaviorTree* BehaviorTree;

	// trigger box component
	UPROPERTY(EditAnywhere, Category = "AI")
	class UBoxComponent* AttackRangeBox;

private:
	UFUNCTION()
	void OnSeePlayer(APawn* Pawn);

	UFUNCTION()
	void OnEnterRange(UPrimitiveComponent * OverlappedComp, AActor * OtherActor,
		UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
	void OnExitRange(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, 
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void AttackPlayer();

	// Enemy stat variables
	UPROPERTY(EditAnywhere, Category = "Stats")
		float InitialHealth;
	UPROPERTY(EditAnywhere, Category = "Stats")
		float CurrentHealth;
	UPROPERTY(EditAnywhere, Category = "Stats")
		float EnemyDamage;

	bool bIsInRange = false;

public:
	// Public accessor functions
	// Health functions
	UFUNCTION(BlueprintPure, Category = "Stats")
		float GetInitialHealth();
	UFUNCTION(BlueprintPure, Category = "Stats")
		float GetCurrentHealth();
	UFUNCTION(BlueprintCallable, Category = "Stats")
		void UpdateCurrentHealth(float Health);
	UFUNCTION(BlueprintCallable, Category = "Death")
		void EnemyDeath();

	// Damage functions
	UFUNCTION(BlueprintPure, Category = "Stats")
		float GetEnemyDamage();

protected:
	FTimerHandle DeathDelayTH;
	FTimerHandle AttackTH;
};
