// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Components/InputComponent.h"
#include "CameraControl.generated.h"

UCLASS()
class GAM312FINAL_API ACameraControl : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACameraControl();
	APlayerController* MyPlayerController;

	// Cameras
	UPROPERTY(EditAnywhere)
		AActor* FirstPersonCamera;
	UPROPERTY(EditAnywhere)
		AActor* ThirdPersonCamera;


	// Functions
	void SetCameraOne();
	void SetCameraTwo();

	virtual void BeginPlay() override;
};
