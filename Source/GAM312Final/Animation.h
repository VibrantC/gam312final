// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Animation/AnimInstance.h"
#include "Animation.generated.h"

/**
 * 
 */
UCLASS(transient, Blueprintable, hideCategories=AnimInstance, BlueprintType)
class GAM312FINAL_API UAnimation : public UAnimInstance
{
	GENERATED_BODY()

public:
	UAnimation(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
	bool TakingDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack")
	bool Firing;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy")
	bool Attacking;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy")
	bool Dead;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy")
	float Speed;
};
